package agenda;

import java.util.Scanner;

public class Agenda {

	Scanner entrada = new Scanner(System.in);

	String nombre;
	int telefono;
	Contactos contactos = new Contactos();

	public void menu() {

		int opcion = entradaDeEntero();

		switch (opcion) {
		case 1:
			agregarContacto();
		case 2:
			mostrarContactos();
		case 3:
			borrarContacto();
		default:
			System.out.println("Seleccione una opcion correcta");
			menu();
		}
	}

	private int entradaDeEntero() {
		return entrada.nextInt();
	}

	private String entradaDeString() {
		return entrada.nextLine();
	}

	private void agregarContacto() {
		System.out.println("nombre del contacto: ");
		String nombreContacto = entradaDeString();

		System.out.println("edad del contacto: ");
		int telefonoContacto = entradaDeEntero();

		contactos.agregarContacto(new Contacto(nombreContacto, telefonoContacto));
		System.out.println("Contacto agregado con exito");

		iterarMenu();
	}

	private void borrarContacto() {
		System.out.println("nombre del contacto que quiere borrar: ");
		String nombreContacto = entradaDeString();
		contactos.borrarContacto(nombreContacto);
		System.out.println("Contacto borrado con exito");

		iterarMenu();
	}

	private void mostrarContactos() {
		System.out.println(contactos.getContactos());
		iterarMenu();
	}

	private void iterarMenu() {
		System.out.println("\nDesea realizar otra operacion?");
		System.out.println("1-Si\n2-No");

		int opcion = entradaDeEntero();
		switch (opcion) {
		case 1:
			menu();
		case 2:
			System.out.println("Gracias por haber utilizado la agenda");
		default:
			System.out.println("Seleccione una opcion correcta");
			iterarMenu();
		}
	}

}
