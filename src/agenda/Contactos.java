package agenda;

import java.util.ArrayList;

public class Contactos {

	ArrayList<Contacto> contactos = new ArrayList<Contacto>();

	public ArrayList<Contacto> getContactos() {
		return contactos;
	}

	public void setContactos(ArrayList<Contacto> contactos) {
		this.contactos = contactos;
	}

	public void agregarContacto(Contacto contacto) {
		contactos.add(contacto);
	}

	public Contacto buscarContacto(String nombreContacto) {
		for (Contacto c : contactos) {
			if (c.getNombre().equals(nombreContacto))
				return c;
		}
		return null;
	}

	public void borrarContacto(String nombreContacto) {
		contactos.remove(buscarContacto(nombreContacto));
	}

}
